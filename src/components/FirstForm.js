import React, { useState } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faIdCard } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';

library.add(faIdCard);

export default function FirstForm() {
  const [name, setName] = useState('');
  const [mobile_no, setMobileNumber] = useState('');
  const [age, setAge] = useState('');
  const [gender, setGender] = useState('');
  const [address, setAddress] = useState('');
  const [pincode, setPincode] = useState('');
  const [sector_product, setSectorProduct] = useState('');
  const [no_farmer, setNoFarmer] = useState('');
  
  const [major_product, setMajorProduct] = useState([
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
  ]);

  const [preffered_vehicle, setPreferredVehicle] = useState('');
  const [small_vehicle, setSmallVehicle] = useState('');
  const [pickup_truck, setPickupTruck] = useState('');
  const [medium_vehicle, setMediumVehicles] = useState('');
  const [driver, setDriver] = useState('');
  const [license_available, setLicense_available] = useState('');
  const [driving_license, setDrivingLicense] = useState('');
  const [trip_per_month, setTripsPerMonths] = useState('');
  const [km_per_month, setKmsPerMonth] = useState('');
  const [driver_experience, setDriverExperience] = useState('');
  const [agriculture_collection, setAgricultureCollection] = useState('');
  const [trading_of_goods, setTradingGoods] = useState('');
  const [showTextArea, setShowTextArea] = useState(false);
  const [showUploadArea, setShowUploadArea] = useState(false);
  // const [textareaValue, setTextareaValue] = useState('');
  const [trading_invoice, setTradingInvoice] = useState('');
  const [about_trading, setAboutTrading] = useState('');
  const [volume_trade_annually, setVolumeTrade] = useState('');
  
  const [kind_of_trade, setKindOfTrade] = useState('');
  const [vehicle_trading, setVehicleTrading] = useState('');
  const [preferred_trading, setPreferredTrading] = useState('');
  const [pay_for_truck, setPayForTruck] = useState('');
  const [have_bank_account, setHaveBankAccount] = useState('');
  const [defaulter, setDefaulter] = useState('');
  const [pan_card, setPanCard] = useState('');
  const [valid_document, setValidDocument] = useState('');
  const [bank_statement, setBankStatement] = useState('');
  const [ovd, setOvd] = useState('');

 

  //show the text area og Q20
  const handleTradingGoodsChange = (e) => {
    setTradingGoods(e.target.value);
    setShowTextArea(e.target.value === 'Yes, agriculture and allied sector products' || e.target.value === 'Yes, non-agriculture products');
  }; 
  //show the upload area and text in Q30
  const handleUploadOvd = (e) => {
    const value = e.target.value;
    if (value === 'other') {
      setShowUploadArea(true);
      setValidDocument('');
    } else {
      setShowUploadArea(false);
      setValidDocument(value);
    }
  };

  const handleOtherInputChange = (event) => {
    const value = event.target.value;
    setValidDocument(value);
  };

  // const handleTextAreaChange = (e) => {
  //   setTextareaValue(e.target.value);
  // };

  const [name_market_location, setNameMarketLocation] = useState([
    { name: '', location: '' }
  ]);
  const [no_harvest_vehicle, setNoHarvestVehicle] = useState({
    farming: false,
    villagers: false,
    other: false,
  });

  //if click other in section 16
  const [otherText, setOtherText] = useState(''); 
  const handleCheckbox = (checkboxName) => {
    if (checkboxName === 'other') {
      setNoHarvestVehicle(prev => ({
        ...prev,
        other: !prev.other,
        // If "Other" checkbox is checked, uncheck "farming" and "villagers"
        farming: false,
        villagers: false,
      }));

      if (!no_harvest_vehicle.other) {
        setOtherText('');
      }
    } else {
      // If "Other" checkbox is checked, prevent checking "farming" and "villagers"
      if (no_harvest_vehicle.other && (checkboxName === 'farming' || checkboxName === 'villagers')) {
        return;
      }

      setNoHarvestVehicle(prev => ({
        ...prev,
        [checkboxName]: !prev[checkboxName],
      }));
    }
  };


  //change if select other in section 16
  const handleOtherTextChange = event => {
    setOtherText(event.target.value);
  };


  // add more fiels in section 8
  const addInputField = () => {
    setNameMarketLocation([...name_market_location, { name: '', location: '' }]);  
  }

  // section 8 market name and location
  const handleInputChange = (index, field, event) => {
    const updatedInputFields = [...name_market_location];
    updatedInputFields[index][field] = event.target.value;
    setNameMarketLocation(updatedInputFields);
  };

  // major product to put in array
  const handleInputProduct = (index, field, event) => {
    const updatedProducts = [...major_product];
    updatedProducts[index][field] = event.target.value;
    setMajorProduct(updatedProducts);
  };

  //section 12 license show if select no
  const [showMessage, setShowMessage] = useState(false);
  const handleLicenseChange = (e) => {
    const selectedValue = e.target.value;
    setLicense_available(selectedValue);
    setShowMessage(selectedValue === 'No');
  };


  //upload licence photo
  const handlePhotoChange = (e) => {
    const selectedPhoto = e.target.files[0];
    if (selectedPhoto) {
        const reader = new FileReader();
        reader.onloadend = () => {
          const setString = reader.result;
          setDrivingLicense(setString);
        };
  
        reader.readAsDataURL(selectedPhoto);
        setDrivingLicense(selectedPhoto);
      }
    }
    
    //upload Trading invoice
    const handlePhotoUpload = (e) => {
      const selectedPhoto = e.target.files[0];
      if (selectedPhoto) {
          const reader = new FileReader();
          reader.onloadend = () => {
            const setString = reader.result;
            setTradingInvoice(setString);
          };
    
          reader.readAsDataURL(selectedPhoto);
          setTradingInvoice(selectedPhoto);
        }
      }


      //upload PAn card
    const handlePanCardUpload = (e) => {
      const selectedPhoto = e.target.files[0];
      if (selectedPhoto) {
          const reader = new FileReader();
          reader.onloadend = () => {
            const setString = reader.result;
            setPanCard(setString);
          };
    
          reader.readAsDataURL(selectedPhoto);
          setPanCard(selectedPhoto);
        }
      }


        //upload Ovd
    const handleOvdUpload = (e) => {
      const selectedPhoto = e.target.files[0];
      if (selectedPhoto) {
          const reader = new FileReader();
          reader.onloadend = () => {
            const setString = reader.result;
            setOvd(setString);
          };
    
          reader.readAsDataURL(selectedPhoto);
          setOvd(selectedPhoto);
        }
      }


      
        //upload bank Statement
    const handleBankStatementUpload = (e) => {
      const selectedPhoto = e.target.files[0];
      if (selectedPhoto) {
          const reader = new FileReader();
          reader.onloadend = () => {
            const setString = reader.result;
            setBankStatement(setString);
          };
    
          reader.readAsDataURL(selectedPhoto);
          setBankStatement(selectedPhoto);
        }
      }


   
    //occupation sec in section 3
  const [occupation, setOccupation] = useState({
    farmer: false,
    govt: false,
    driver: false,
    none: false,
  });
  const handleCheckboxChange = (event) => {
    const { name, checked } = event.target;
    setOccupation((prevValues) => ({
      ...prevValues,
      [name]: checked,
    }));
  };




  const handleSubmit = async (e) => {
    e.preventDefault();

    // const formattedNameMarketLocation = name_market_location.map(item => [item.name, item.location]);

    const formattedNameMarketLocation = [];

      name_market_location.forEach(item => {
        const innerArray = [item.name, item.location];
        formattedNameMarketLocation.push(innerArray);
      });

 // Filter out items with empty names
const filteredMajorProducts = major_product.filter(item => item.name.trim() !== '');

// Extract non-empty names from the filtered objects and format as an array of arrays
const formattedNames = filteredMajorProducts.map(item => [item.name]);
      // major_product.forEach(item => {
      //   const innerArray = [item.name];
      //   formattedNameMarketLocation.push(innerArray);
      // });
      

    // const filteredNameMarketLocation = name_market_location.map(item => (
    //   `{${item.name} - ${item.location}}` // Adjust this based on how you want to format the value
    // ));
    const requiredConditionMet = license_available !== 'No'; //license

    const selectedOccupations = Object.keys(occupation).filter(key => occupation[key]); //occupation

    const selectedHarvestVehicle = Object.keys(no_harvest_vehicle)
    .filter(key => key !== 'other' && no_harvest_vehicle[key])
    .concat(no_harvest_vehicle.other ? [otherText] : []);
    
    const data = { name, mobile_no, age, address, pincode, occupation:selectedOccupations , 
      gender, sector_product, no_farmer, major_product: formattedNames, agriculture_collection,
      preffered_vehicle, small_vehicle, pickup_truck, medium_vehicle, driver, license_available: requiredConditionMet,
      driving_license, name_market_location: formattedNameMarketLocation, driver_experience, trip_per_month, km_per_month,
      no_harvest_vehicle: selectedHarvestVehicle, trading_invoice, about_trading, volume_trade_annually, trading_of_goods,
      kind_of_trade, vehicle_trading, preferred_trading, pay_for_truck, defaulter, pan_card, valid_document, bank_statement,
      ovd,have_bank_account
    };

      console.log(data);
   

    // fetch('https:/dae1-103-74-169-254.ngrok-free.app/backend/model/create.php', {
    //         method: 'POST',
    //         headers: { "Content-Type": "application/json" },
    //         body: JSON.stringify(data)
    //     }).then (() => {
    //         alert('New user is added successfully')
           
           
    //     });

    try {
      const response = await axios.post('https://1b1e-103-74-169-231.ngrok-free.app/backend/model/create.php', data); // Replace 'API_ENDPOINT_URL' with the actual API endpoint
      console.log('Data sent:', response.data);
    } catch (error) {
      console.error('Error sending data:', error);
    }
  };

  return (

    <div className=" grid grid-cols-2 mt-9 mr-80">
      <div className=' bg-gray-500 overflow-hidden p-10 relative text-center ml-80 w-72 rounded'>
        <h1 className=" font-extrabold text-slate-100">
        <FontAwesomeIcon icon="fa-solid fa-id-card" size="xl" style={{color: "#f7b1ab",}} className="mr-2" />

        REGISTER
        </h1>
      </div>
      <div className=" box-decoration-slice bg-red-200 grid box-border rounded">
       
      <form onSubmit={handleSubmit} encType='multipart/form-data'>
      <header>



        {/* section1 */}
        <div className=" section1 flex justify-between m-2">
          <div className="name ml-14 mt-1">
        <label htmlFor="pname" className=" block mr-36 font-mono" >Name</label> 
        <input
          type="text"
          value={name} className=" rounded-md placeholder:font-extralight border-2 border-transparent w-48 pl-2 text-center"
          placeholder='Name'
          onChange={(e) => setName(e.target.value)}
        />{' '} 
     
        </div>
        <div className="mobile mr-14">
        <label htmlFor="pmobileno" className="  font-mono mr-28">Mobile</label> <br />
        <input
          type="number"
          value={mobile_no} className=' rounded placeholder:font-extralight border-2 border-transparent w-48 pl-2 text-center'
          placeholder='Mobile Number'
          onChange={(e) => setMobileNumber(e.target.value)}
        />{' '}
       <br />
       </div>
        </div>



        {/* section2 */}
        <div className="section2 flex justify-between m-2">

            {/* gendersec */}
          <div className="gendersec box-border ml-14 mt-1">
            <label htmlFor="pgender" className="block mr-32 font-mono">Gender</label>
            <div className='radiocontainer bg-white rounded p-2 border-2 border-transparent w-40 flex'>
              <input type="radio" 
              value="female" 
              name="gender" 
              className=" " 
              onChange={(e) => setGender(e.target.value)}/>{' '}
              <label htmlFor="fgender" className="mr-7 font-sans block">Female</label>
              <input type="radio" 
              value="male"
              name="gender" 
              className="" 
              onChange={(e) => setGender(e.target.value)}/>{' '}
              <label htmlFor="mgender" className=' font-sans block'>Male</label>
            </div>
          </div>

            {/* agesec */}
          <div className="agesec mr-14 mt-2">
            <label htmlFor="page" className=" block font-mono mr-32">Age</label>
            <input
              type="number"
              value={age} className=" rounded-md placeholder:font-extralight placeholder:text-center border-2 border-transparent w-48 pl-2 text-center"
              placeholder='Age'
              onChange={(e) => setAge(e.target.value)}
            />{' '}
          </div>

        </div>



        {/* section3 */}
        <div className=" section3 flex justify-between m-2">
          
          {/* addresssec */}
          <div className="addressec ml-14 mt-1">
            <div className=''>
              <label htmlFor="paddress" className=" block mr-32 font-mono" >Address</label> 
              <textarea name="address" id="address" cols="30" rows="10"
                  value={address} className=" rounded-md placeholder:font-extralight border-2 border-transparent w-48 pl-2 h-20"
                  onChange={(e) => setAddress(e.target.value)}/>{' '}
            </div>

              {/* pinsec */}
            <div className=' pinsec grid mt-2'>
              <label htmlFor="ppin" className=" mr-32 font-mono ">Pin Code</label>
              <input
              type="number"
              value={pincode} className=" rounded placeholder:font-extralight placeholder:text-center border-2 border-transparent"
              placeholder='Pin Code'
              onChange={(e) => setPincode(e.target.value)}
            />{' '}
            </div>
          </div>

            {/* occupationsec */}
        <div className="occupationsec box-border mt-1 mr-10 ">
            <label htmlFor="poccupation" className="block mr-20 font-mono">Occupation</label>
            <div className=' bg-white rounded p-2 border-2 border-transparent w-56 h-36 flex'>

              {/* all of the checkbox */}
            <div className='block ml-6'>
              <input 
              type="checkbox" 
              name="farmer" 
              className=" " 
              checked={occupation.farmer}
              onChange={handleCheckboxChange}
              />
              <label htmlFor="foccupation" className=" mb-4 font-sans block">Farmer</label>

              <input 
              type="checkbox"           
              name="govt" 
              className="" 
              checked={occupation.govt}
              onChange={handleCheckboxChange}/>
              <label htmlFor="goccupation" className=' font-sans block'>Govt job</label><br />
            </div>

            <div className=' block ml-10'>
              <input 
              type="checkbox"           
              name="driver" 
              className=" " 
              checked={occupation.driver}
              onChange={handleCheckboxChange}
              />
              <label htmlFor="foccupation" className=" mb-4 font-sans block">Driver</label>

              <input 
              type="checkbox"           
              name="none" 
              className="" 
              checked={occupation.none}
              onChange={handleCheckboxChange}/>
              <label htmlFor="goccupation" className=' font-sans block'>None</label>

              </div>
            </div>
          </div>        
        </div>


        {/* section4 */}
        <div className='section4  m-2 mt-7'>
            {/* MajorProductsec */}
           
            <div className="otherq mt-5 ">
                  <label htmlFor="otherq" className="font-mono mr-28">
                  What are the major products in your area?
                  </label> <br />
                  
                  <div className=' inputsection mr-48 mt-2' >
                  {major_product.map((product, index) => (
                  <div className={`number ${index + 1} mt-2`} key={index} >
                    <label htmlFor={index + 1}>{index + 1}.</label>
                  <input
                    type="text"
                    className="rounded placeholder:font-extralight border-2 border-transparent w-56 pl-2  mr-9"
                    placeholder="Type here"
                    value={product.name}
                    onChange={(event) => handleInputProduct(index, 'name', event)}                   
                    />
                  </div>
                    ))}



                  
                
                </div>
                
            </div>  
           
        </div>
      

        {/* section5 */}
        <div className='secition5  m-2 mt-7'>

        <div className='agriculturecollectionsec ml-11 flex'>
              <div className=' grid  text-left'>
              <label htmlFor="agriculturecollection" className=" font-mono">How many agricultures collection </label>
              <label htmlFor="agriculturecollection2" className=" font-mono">centres/ aggregation points are there in your block?</label>
              </div>
              <div className=' mr-24'>
                {/* select */}
              <select name="agriculturecollection" id="agriculturecollection" 
              className=' rounded text-center'
              value={agriculture_collection}
              onChange={(e) => setAgricultureCollection(e.target.value)}
              >
                <option value="Select" className=' hidden ' >--Select--</option>
                <option value="One">One</option>
                <option value="Two">Two</option>
                <option value="Three">Three</option>
                <option value="Four">Four</option>
                <option value=">Five">&gt;Five</option>
              </select>
              </div>
            </div>
        </div>

         {/* section6 */}
        <div className="section6 mt-10">

        <div className="transportsec mx-9">
            <div className=''>
              <label htmlFor="pquestion" className=" block font-mono" >How do farmers currently transport their agriculture and allied sector products?</label> 
              <textarea name="panswer" id="panswer" cols="30" rows="10"
                  value={sector_product} className=" rounded-md  border-2 border-transparent h-20 w-11/12 mt-4 pl-2"
                  onChange={(e) => setSectorProduct(e.target.value)}/>{' '}
            </div>
          </div>
        </div>


          {/* section7 */}
        <div className='section7  m-2 mt-7'>
            {/* productsec */}
            <div className='productsec flex text-left ml-10'>
              <div className=' grid'>
              <label htmlFor="products" className=" font-mono">Number of farmers in your area</label>
              <label htmlFor="products2" className=" font-mono">to whom you can provide your transportation services?</label>
              </div>
              <div className=' mr-12'>
                {/* Enter number */}
                
                  <input
                    type="number"
                    value={no_farmer} className=" rounded-md placeholder:font-extralight placeholder:text-center border-2 border-transparent w-32 pl-2 text-left"
                    placeholder='Enter a number'
                    onChange={(e) => setNoFarmer(e.target.value)}
                  />{' '}
              </div>
            </div>
        </div>


          {/* section8 */}
          <div className="section8 m-2 mt-7">
      <div className="">
        <label htmlFor="market" className="block font-semibold">
          Market in Your Area
        </label>

            {/* market location */}
          {name_market_location.map((name_location, index) => (
            <div className="flex justify-between" key={index}>
              <div className="namemarket ml-9 mt-1">
                <label htmlFor="pname" className="block font-mono text-left">
                  Name of the Market
                </label>
                <input
                  type="text"
                  className="rounded-md placeholder:font-extralight border-2 border-transparent w-48 pl-2 text-center"
                  placeholder="Market Name"
                  value={name_location.name}
                  onChange={(event) => handleInputChange(index, 'name', event)}
                />
              </div>


                {/* location */}
                <div className="location mr-14">
                  <label htmlFor="plocation" className="font-mono mr-28">
                    Location
                  </label> <br />
                  <input
                    type="text"
                    className="rounded placeholder:font-extralight border-2 border-transparent w-48 pl-2 text-center"
                    placeholder="Location"
                    value={name_location.location}
                    onChange={(event) => handleInputChange(index, 'location', event)}
                  />{' '}
                  <br />
                </div>
              </div>
            ))}
    <button className="border-2 border-cyan-400" onClick={addInputField}>
      Add
    </button>
  </div>
</div>

      



        
          {/* section9 */}
          <div className="section9 m-2 mt-7">

            <div className=' '> 
            <label htmlFor="vehicle" className='block  font-semibold mb-4'>What kind of vehicle will you prefer?</label> 

            <div className='flex justify-between'>

            {/* smallvehicle */}
            <div className="namemarket ml-9">
            <label htmlFor="smallvehicle" className=" block font-mono text-left mt-1" >Small Commercial vehicles</label> 
            {/* select */}
            <select name="smallvehicle" id="smallvehicle" 
              className=' rounded text-center'
              value={small_vehicle}
              onChange={(e) => setSmallVehicle(e.target.value)}
              >
                <option value="Select" className=' hidden ' >--Select--</option>
                <option value="Suzuki Super Carry">Suzuki Super Carry </option>
                <option value="Tata Ace Petrol CX">Tata Ace Petrol CX</option>
                <option value="Tata Ace Diesel+">Tata Ace Diesel+</option>
                <option value="Tata Intra V10">Tata Intra V10</option>
                <option value="Tata Intra V30">Tata Intra V30</option>
                <option value="Ashok Leyland Dost Plus LS HSD">Ashok Leyland Dost Plus LS HSD</option>
                <option value="Ashok Leyland Bada Dost i4 LS FSD">Ashok Leyland Bada Dost i4 LS FSD</option>
              </select>


                {/* Pickup Truck */}
              </div>
                  <div className="location mr-14">
                    <label htmlFor="pickuptruck" className="  font-mono ">Pickup Trucks</label> <br />
                    {/* select */}
                      <select name="pickuptruck" id="pickuptruck" 
                        className=' rounded text-center'
                        value={pickup_truck}
                        onChange={(e) => setPickupTruck(e.target.value)}
                        >
                          <option value="Select" className=' hidden ' >--Select--</option>
                          <option value="Tata Yodha 1700 2WD">Tata Yodha 1700 2WD</option>
                          <option value="Tata Yodha SC 4WD">Tata Yodha SC 4WD</option>
                          <option value="Bolero Maxi Truck PS 2WD">Bolero Maxi Truck PS 2WD</option>
                          <option value="Big Bolero Pik Up 1700">Big Bolero Pik Up 1700</option>
                          <option value="Bolero Pick up 4WD  MS">Bolero Pick up 4WD  MS</option>
                        </select>
                  </div>
              </div>

              {/* Medium Commercial vehicles */}
              <div className=' mt-6'>
              <label htmlFor="mediumvehicle" className="font-mono ">Medium Commercial vehicles</label> <br />
                    {/* select */}
                      <select name="mediumvehicle" id="mediumvehicle" 
                        className=' rounded text-center'
                        value={medium_vehicle}
                        onChange={(e) => setMediumVehicles(e.target.value)}
                        >
                          <option value="Select" className=' hidden ' >--Select--</option>
                          <option value="Eicher Pro 2050">Eicher Pro 2050</option>
                          <option value="SML Isuzu Sartaj GS HG75">SML Isuzu Sartaj GS HG75</option>
                          <option value="Bharat Benz___"> Bharat Benz___</option>
                          <option value="Tata_____"> Tata_____</option>
                          <option value="Ashok Leyland">Ashok Leyland</option>
                        </select>
              </div>


              </div>          
          </div>



          {/* section10 */}
           
        <div className="section10 mt-10">

        <div className="preferedvehiclesec mx-9">
              <label htmlFor="pquestion" className=" block font-mono" >Please provide justification for the above preferred vehicle</label> 
              <textarea name="panswer" id="panswer" cols="30" rows="10"
                  value={preffered_vehicle} className=" rounded-md  border-2 border-transparent h-14 w-11/12 mt-4 pl-2"
                  onChange={(e) => setPreferredVehicle(e.target.value)}/>{' '}
            
          </div>
        </div>


          {/* section11 */}
          <div className='section11  m-2 mt-7'>
            {/* MajorProductsec */}
            <div className='driversec ml-10 flex'>
              <div className=' grid'>
              <label htmlFor="pquestion" className=" font-mono text-left">Who will be the driver of the vehicle?</label>
              </div>
              <div className=' mr-10'>
                {/* selectproduct */}
              <select name="answer" id="answer" 
              className=' rounded text-center'
              value={driver}
              onChange={(e) => setDriver(e.target.value)}
              >
                <option value="Select" className=' hidden ' >--Select--</option>
                <option value="Self (Owner)">Self/Owner</option>
                <option value="Will hire a driver on salary">Will hire a driver on salary</option>
        
              </select>
              </div>
            </div>
        </div>



               {/* section12 */}
        <div className='secition12  m-2 mt-7'>

        <div className='agriculturecollectionsec ml-11 flex'>
              <div className=' grid  text-left'>
              <label htmlFor="agriculturecollection" className=" font-mono">Do you or your driver has commercial driving license</label>
              <label htmlFor="agriculturecollection2" className=" font-mono">or can you hire a driver with a commercial driving license?</label>
              </div>
              <div className=' mr-24'>
                  {/* select */}
                <select name="agriculturecollection" id="agriculturecollection" 
                className=' rounded text-center'
                value={license_available}
                onChange={handleLicenseChange}
                >
                  <option value="Select" className=' hidden ' >--Select--</option>
                  <option value="Yes" >Yes</option>
                  <option value="No">No</option>
                </select>
              </div>
          </div>
          {showMessage && (
            <div className="ml-11 text-red-600">
              (Driving license is necessary)
            </div>
          )}
        </div>




              {/* section13 */}
              <div className='secition12  m-2 mt-7'>

            <div className='uploadsec ml-11 flex'>
                  <div className=' grid  text-left'>
                  <label htmlFor="agriculturecollection" className=" font-mono">Kindly attach the driving license</label>
                  </div>
                  <div className='ml-8'>
                    {/* upload license */}
                    <input type="file" 
                    accept="image/*"
                    className='upload license '
                    onChange={handlePhotoChange}
                       />
                  </div>
                </div>
            </div>



              {/* section14 */}
        <div className='secition14  m-2 mt-7'>

        <div className='Experiencesec ml-11 flex'>
              <div className=' grid  text-left mr-4'>
              <label htmlFor="Experience" className=" font-mono">How many years of experience do you or your driver has as a commercial driver?</label>
              </div>
              <div className=' mr-11'>
                {/* select Experience */}
              <select name="Experience" id="Experience" 
              className=' rounded text-center'
              value={driver_experience}
              onChange={(e) => setDriverExperience(e.target.value)}
              >
                <option value="Select" className=' hidden ' >--Select--</option>
                <option value="None">None</option>
                <option value="1-3 years">1-3 years</option>
                <option value="3-5 years">3-5 years</option>
                <option value="5-7 years">5-7 years</option>
                <option value="7-10 years">7-10 years</option>
                <option value="More than 10 years">More than 10 years</option>

              </select>
              </div>
            </div>
        </div>


            {/* section15 */}
            <div className="section15 m-2 mt-7">
            <div className="">
              <label htmlFor="trips" className="block font-semibold">
              How many trips can you do in a month on average? <br></br>
              (Agriculture transportation-related trips only)
              </label>

                  {/* Trips per Months */}
                
                  <div className="flex justify-between mt-3">
                    <div className="trip ml-9 mt-1">
                      <label htmlFor="ptrip" className="block font-mono text-left">
                    1.Average number of trips <br /> /months
                      </label>
                      <input
                        type="number"
                        className="rounded-md placeholder:font-extralight border-2 border-transparent w-48 pl-2 text-center mt-1"
                        placeholder="Trips per Months"
                        value={trip_per_month}
                        onChange={(e) => setTripsPerMonths(e.target.value)}
                      />{' '}
                    </div>


                {/* kms per month */}
                <div className="kmspermonths mr-14">
                  <label htmlFor="pkms" className="font-mono">
                  2.Average kms / month
                  </label> <br />
                  <input
                    type="number"
                    className="rounded placeholder:font-extralight border-2 border-transparent w-48 pl-2 text-center mt-7"
                    placeholder="kms per month"
                    value={km_per_month}
                    onChange={(e) => setKmsPerMonth(e.target.value)}                  
                    />{' '}
                  <br />
                </div>
              </div>
          
  
          </div>
        </div>



           {/* section16 */}
           <div className=" section16 flex justify-between m-2">

            {/* no_harvestsec */}
        <div className="noharvestsec box-border mt-9 ml-10">
            <label htmlFor="pnoharvestsec" className="block text-left font-mono">How will you use the vehicle when there is no harvest?</label>
            <div className=' bg-white rounded p-2 border-2 border-transparent w-72 h-32 mt-2 '>

              {/* all of the checkbox */}
         
              <input 
              type="checkbox" 
              name="farming" 
              className="" 
              checked={no_harvest_vehicle.farming}
              onChange={() => handleCheckbox('farming')}
              />
              <label htmlFor="Will get daily essentials from the market for villagers" className=" mb-4 font-sans ml-2">Will get raw materials for farming </label><br />

              <input 
              type="checkbox"           
              name="villagers" 
              value="Will get daily essentials from the market for villagers"
              className=" mr-2 mt-4" 
              checked={no_harvest_vehicle.villagers}
              onChange={() => handleCheckbox('villagers')}
              />
              <label htmlFor="Will get daily essentials from the market for villagers" className=' font-sans mt-4'>Will get daily essentials from the market for villagers</label><br />
            

            <div className=' mr-44'>
              <input 
              type="checkbox"           
              name="other" 
              className=" mt-2" 
              checked={no_harvest_vehicle.other}
              onChange={() => handleCheckbox('other')}
              />
              <label htmlFor="other" className=" font-sans ml-3 mt-2">Other</label>

        
              </div>
            </div>
          </div>


            {/* other q */}
            {no_harvest_vehicle.other && (
                <div className="otherq mr-8  mt-8">
                  <label htmlFor="otherq" className="font-mono">
                  If other in the question, please mention
                  </label>
                  <input
                    type="text"
                    className="rounded placeholder:font-extralight border-2 border-transparent w-48 pl-2 text-center mt-5"
                    placeholder="Mention here"
                    value={otherText}
                    onChange={handleOtherTextChange}                  
                    />
                  <br />
                </div>  
                )}      
        </div>



                 {/* section17 */}
        <div className='secition17  m-2 mt-7'>

          <div className='Tradingsec ml-11 '>
                <div className='   text-left mr-4'>
                <label htmlFor="Trading" className=" font-mono">Have you done trading of any goods in the past?</label> 
                </div>
                <div className='  pr-40'>
                  {/* select Trading goods */}
                <select name="Trading" id="Trading" 
                className=' rounded text-center mt-2 mr-14'
                value={trading_of_goods}
                onChange={handleTradingGoodsChange}
                >
                  <option value="Select" className=' hidden ' >--Select--</option>
                  <option value="Yes, agriculture and allied sector products" id='Yes'>Yes, agriculture and allied sector products</option>
                  <option value="Yes, non-agriculture products" id='Yes'> Yes, non-agriculture products</option>
                  <option value="No" id='No'>No</option>
                </select>
                </div>
          </div>

              {/* Conditional rendering for the field area */}
              {/* about Trading */}
                {showTextArea && (
                <div className="otherq mt-5 ">
                  <label htmlFor="otherq" className="font-mono mr-52">
                  If yes, please tell us about it
                  </label> <br />
                  <input
                    type="text"
                    className="rounded placeholder:font-extralight border-2 border-transparent  w-3/4 pl-2 h-12 mr-14"
                    placeholder="Type here"
                    value={about_trading}
                    onChange={(e) => setAboutTrading(e.target.value)}                   
                    />
                </div>                           
                )}


                  {/* Anually Trading Volume */}
                {showTextArea && (
                  <div className="otherq mt-5">
                  <label htmlFor="otherq" className="font-mono text-left mr-2">
                  If yes, what is your volume of trade annually in Tonnes <br /> (Also mention the type of products)
                  </label> <br />
                  <input
                    type="text"
                    className="rounded placeholder:font-extralight border-2 border-transparent  w-3/4 pl-2 h-12 mr-14"
                    placeholder="Type here"
                    value={volume_trade_annually}
                    onChange={(e) => setVolumeTrade(e.target.value)}                  
                    />
                </div>  
                )}


                  {/* upload image  */}
                {showTextArea && (
                  <div className="otherq mt-5">
                  <label htmlFor="otherq" className="font-mono text-left mr-20">
                  If yes, please attach any trading invoice copy.
                  </label> <br />
                  <input
                    type="file"
                    className="rounded placeholder:font-extralight border-2 border-transparent  w-3/4 pl-2 h-12 mr-14 mt-2"
                    accept="image/*"
                    onChange={handlePhotoUpload}                
                    />
                </div>  
                )}

                 {/* kind of Trading */}
                {showTextArea && (
                  <div className="otherq mt-2">
                  <label htmlFor="otherq" className="font-mono mr-40">
                  If yes, what kind of trade do you do?
                  </label> <br />
                  <select name="qtrading" id="qtrading" 
                  className='rounded text-center mt-2 mr-80'
                  value={kind_of_trade}
                  onChange={(e) => setKindOfTrade(e.target.value)}
                  >
                    <option value="Select" className='hidden'>--Select--</option>
                    <option value="Inter-district trade"> Inter-district trade</option>
                    <option value="Inter-state trade">Inter-state trade</option>
                    <option value="International trade">International trade</option>
                    <option value="None">None</option>
                  </select>
                </div>  
                )}

                    {/* Vehicle Trading */}
                {showTextArea && (
                  <div className="otherq mt-4">
                  <label htmlFor="otherq" className="font-mono mr-40">
                  If yes, the vehicle used for trading
                  </label> <br />
                  <select name="qtrading" id="qtrading" 
                  className='rounded text-center mt-2 mr-60'
                  value={vehicle_trading}
                  onChange={(e) => setVehicleTrading(e.target.value)}
                  >
                    <option value="Select" className='hidden'>--Select--</option>
                    <option value="I use my own vehicle for trading">I use my own vehicle for trading</option>
                    <option value="I hire a vehicle for trading">I hire a vehicle for trading</option>
                    <option value=" I hire from 1917iTeams vehicles"> I hire from 1917iTeams vehicles</option>
                  </select>
                </div>  
                )}

                  {/* Preferred Trading */}
                {showTextArea && (
                  <div className="otherq mt-4">
                  <label htmlFor="otherq" className="font-mono -ml-8 text-left mr-10">
                  If yes, which is your preferred use for the trading <br/>of the products above?
                  </label> <br />
                  <select name="qtrading" id="qtrading" 
                  className='rounded text-center mt-2 mr-64'
                  value={preferred_trading}
                  onChange={(e) => setPreferredTrading(e.target.value)}
                  >
                    <option value="Select" className='hidden'>--Select--</option>
                    <option value="Small commercial vehicle">Small commercial vehicle</option>
                    <option value="2WD pickup trucks">2WD pickup trucks</option>
                    <option value="4WD pickup trucks">4WD pickup trucks</option>
                    <option value="Medium Commercial Vehicle">Medium Commercial Vehicle</option>
                    <option value="Heavy Commercial Vehicle">Heavy Commercial Vehicle</option>
                  </select>
                </div>  
                )}
                
                    {/* PAy For Truck */}
                {showTextArea && (
                <div className="otherq mt-5 ">
                  <label htmlFor="otherq" className="font-mono -ml-16">
                  If yes, on an average, how much do you normally <br /> pay for the truck, when you hire? <br /> (Total amount you pay in rupees/ total weight you transported in kg) 
                  </label> <br />
                  <input
                    type="number"
                    className="rounded placeholder:font-extralight border-2 border-transparent  w-3/4 pl-2 h-12 mr-14"
                    placeholder="(Rs/kg)"
                    value={pay_for_truck}
                    onChange={(e) => setPayForTruck(e.target.value)}                 
                    />
                </div>                           
                )}
                
          </div>

              {/* section 18 */}
            <div className='section18  m-2 mt-7'>
            {/* BankAccount */}
            <div className='section18 ml-11 flex'>
              <div className=' grid'>
              <label htmlFor="bank" className=" font-mono">Do you have a bank account?</label>
    
              </div>
              <div className='  ml-28'>
                {/* Q */}
              <select name="bank" id="bank" 
              className=' rounded text-center'
              value={have_bank_account}
              onChange={(e) => setHaveBankAccount(e.target.value)}
              >
                <option value="Select" className=' hidden ' >--Select--</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              
              </select>
              </div>
            </div>
        </div>



          {/* section 19 */}
          <div className='section19  m-2 mt-9'>
            {/* Loan */}
            <div className='section18 ml-11 flex'>
              <div className=' grid'>
              <label htmlFor="loan" className=" font-mono">If you have taken any loan, <br />are you a defaulter?</label>
    
              </div>
              <div className=' ml-28'>
                {/* Q */}
              <select name="loan" id="loan" 
              className=' rounded text-center'
              value={defaulter}
              onChange={(e) => setDefaulter(e.target.value)}
              >
                <option value="Select" className=' hidden ' >--Select--</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              
              </select>
              </div>
            </div>
        </div>
        


             {/* section20 */}
        <div className='secition20  m-2 mt-7'>

        <div className='uploadsec ml-11 flex'>
              <div className=' grid  text-left'>
              <label htmlFor="pan" className=" font-mono">Pan Card copy, if applicable</label>
              </div>
              <div className='ml-8'>
                {/* upload pan */}
                <input type="file" 
                accept="image/*"
                className='upload Pan card '
                onChange={handlePanCardUpload}
                  />
              </div>
            </div>
        </div>


           {/* section21 */}
           <div className='section21  m-2 mt-9'>
                {/* Loan */}
                <div className='section21 ml-3 mr-7 flex'>
                  <div className=' grid'>
                  <label htmlFor="loan" className=" font-mono">Upload Any of the following officially <br />valid documents (OVDs)</label>
        
                  </div>
                  <div className=' ml-7'>
                    {/* Q */}
                  <select name="loan" id="loan" 
                  className=' rounded text-center'
                  value={valid_document}
                  onChange={handleUploadOvd}
                  >
                    <option value="Select" className=' hidden ' >--Select--</option>
                    <option value="Voter ID">Voter ID</option>
                    <option value="Aadhar">Aadhar</option>
                    <option value="Passport">Passport</option>
                    <option value="other">Any other (Please mention)</option>
                  
                  </select>
                  </div>
                </div>

                {showUploadArea && (
                  <div className="otherq mt-5 ">
                  <label htmlFor="otherq" className="font-mono mr-52">
                  If, Any other (Please mention)
                  </label> <br />
                  <input
                    type="text"
                    className="rounded placeholder:font-extralight border-2 border-transparent  w-3/4 pl-2 h-9 mr-14"
                    placeholder="Type here"
                    value={valid_document}
                    onChange={handleOtherInputChange}                   
                    />
                </div>  
                )}

            <div className='uploadsec ml-11 m-2 mt-5 flex'>
              <div className=' grid  text-left'>
              <label htmlFor="agriculturecollection" className=" font-mono">Kindly upload the documents here</label>
              </div>
              <div className='ml-8'>
                {/* upload license */}
                <input type="file" 
                accept="image/*"
                className='upload valid document '
                onChange={handleOvdUpload}
                  />
              </div>
            </div>

        </div>


           {/* section22 */}
           <div className='secition22  m-2 mt-7'>

            <div className='uploadsec ml-11 flex'>
                  <div className=' grid  text-left'>
                  <label htmlFor="bankstatement" className=" font-mono">Latest Bank Statement, if applicable</label>
                  </div>
                  <div className='ml-8'>
                    {/* upload statement */}
                    <input type="file" 
                    accept="image/*"
                    className='upload Bank Statement '
                    onChange={handleBankStatementUpload}
                      />
                  </div>
                </div>
            </div>


        </header>

        {/* footer button submit */}
        <footer className=' bg-white grid items-center py-1 px-10 rounded'>
        <div className=' flex justify-between m-2'>
          <input value="Back" className=' bg-slate-100 rounded-md h-7 w-11 text-center'/>
          <input type="submit" value="Submit" className=" bg-gray-500 rounded-md h-8 w-16 from-neutral-50"/>
        </div>
        </footer>
      </form>
     
      </div>
    </div>
  );
}
