import './App.css';
import FirstForm from './components/FirstForm';

function App() {
  return (
    <div className="App">
      <FirstForm />
    </div>
  );
}

export default App;
